using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovements : MonoBehaviour
{
    public float playerSpeed;
    public float playerJump;
    public Rigidbody2D rb2D;
    Animator animatormovement;
    public LayerMask ground;
    public SpriteRenderer spriteRenderer;
    public int health;

    // Start is called before the first frame update
    void Start()
    {
        health = 1;
        animatormovement = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            rb2D.velocity = new Vector2(-playerSpeed, rb2D.velocity.y);
            spriteRenderer.flipX = true;
            animatormovement.SetBool("Run", true);
        }
        else if (Input.GetKeyUp(KeyCode.A))
        {
            rb2D.velocity = new Vector2(0, rb2D.velocity.y);
            animatormovement.SetBool("Run", false);
        }

        if (Input.GetKey(KeyCode.D))
        {
            spriteRenderer.flipX = false;
            rb2D.velocity = new Vector2(+playerSpeed, rb2D.velocity.y);
            animatormovement.SetBool("Run", true);
        }
        else if (Input.GetKeyUp(KeyCode.D))
        {
            rb2D.velocity = new Vector2(0, rb2D.velocity.y);
            animatormovement.SetBool("Run", false);
        }

        if (Input.GetKey(KeyCode.Space) && Mathf.Abs(rb2D.velocity.y) < 0.001f && IsGrounded())
        {
            rb2D.velocity = new Vector2(rb2D.velocity.x, +playerJump);
            animatormovement.SetBool("Jump", true);
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            rb2D.velocity = new Vector2(rb2D.velocity.x, 0);
            animatormovement.SetBool("Jump", false);
        }
    }

    bool IsGrounded()
    {
        Vector2 position = transform.position;
        Vector2 direction = Vector2.down;
        float distance = 0.8f;
        RaycastHit2D hit = Physics2D.Raycast(position, direction, distance, ground);
        Debug.DrawRay(position, direction, Color.green, 1);

        if (hit.collider != null)
        {
            return true;
        }
        return false;
    }
}
