using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour
{
    Animator animator;
    public GameObject chestItem;
    public float delay;
    public int itemAmount;
    private int itemCount;
    public bool itemPicked;

    // Start is called before the first frame update
    void Start()
    {
        itemPicked = false;
        animator = GetComponent<Animator>();
        animator.SetBool("chest", false);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && !itemPicked)
        {
            if (Input.GetKey(KeyCode.W))
            {
                animator.SetBool("chest", true);
                while (itemCount < itemAmount)
                {
                    Instantiate(chestItem, transform.position, Quaternion.identity);
                    itemPicked = true;
                    itemCount++;
                }
            }
        }
    }
}
