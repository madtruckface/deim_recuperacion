using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class CoinCounter : MonoBehaviour
{
    private void OnEnable()
    {

        GetComponent<Collider2D>().enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.gameObject.CompareTag("Player"))
        {
            ++GameManager.score;
            Destroy(gameObject);
        }
    }
}
