using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GameOverController : MonoBehaviour
{
    public GameObject gameoverpanel;
    public CharacterMovements player;

    // Start is called before the first frame update
    void Start()
    {
        gameoverpanel.SetActive(false);
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (player.health == 0)
        {
            Time.timeScale = 0;
            gameoverpanel.SetActive(true);
        }
    }

    public void Retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
